import React, { Component } from 'react';
import { StyleSheet, Text, View, AppState} from 'react-native';
import RootStack from './app/screens/Nav';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import '@react-native-firebase/dynamic-links';
import AsyncStorage from '@react-native-community/async-storage';

const iosConfig = {
  clientId: '233535255223-nmqq0h110vcphhsrec8sq2mu0tcqj9ci.apps.googleusercontent.com',
  appId: '1:233535255223:ios:1a70fd02e1a6a48b351332',
  apiKey: 'AIzaSyC_lvMKFwVukMg_Fs8bRG_IfATmwDGtmYs',
  databaseURL: 'https://policy-40e8e.firebaseio.com',
  storageBucket: 'policy-40e8e.appspot.com',
  messagingSenderId: '233535255223',
  projectId: 'policy-40e8e',
  persistence: true,
};
 
const androidConfig = {
  clientId: '233535255223-nmqq0h110vcphhsrec8sq2mu0tcqj9ci.apps.googleusercontent.com',
  appId: '1:233535255223:android:a5a390e169b9dd53351332',
  apiKey: 'AIzaSyAFzPbWo1KbaiCnztqNivxvqy7nnSLAW7w',
  databaseURL: 'https://policy-40e8e.firebaseio.com',
  storageBucket: 'policy-40e8e.appspot.com',
  messagingSenderId: 'x',
  projectId: 'policy-40e8e',
  persistence: true,
};

firebase
  .initializeApp(
    Platform.OS === 'ios' ? iosConfig : androidConfig
  )
  .then(app => console.log('initialized apps ->', firebase.apps));

export default class App extends React.Component {
	componentDidMount() {
		firebase.app().dynamicLinks().onLink((link) => {
			if (link != null) {
				AsyncStorage.getItem('@email').then((email) => {
					firebase.app().auth().signInWithEmailLink(email, link.url).then((user) => {
						alert("User successfully logged in!");
					}).catch((e) => {
						alert(e)
					});
				})
			}
		});

		firebase.app().dynamicLinks().getInitialLink().then((link) => {
			if (link != null) {
				AsyncStorage.getItem('@email').then((email) => {
					firebase.app().auth().signInWithEmailLink(email, link.url).then((user) => {
						alert("User successfully logged in!");
					}).catch((e) => {
						alert(e)
					});
				})
			}
		})
	}

	render() {
		return <RootStack />;
	}
}

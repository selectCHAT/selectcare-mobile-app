import React from 'react';
import { View, Text, Button, Image, TouchableOpacity, TextInput, Alert, SafeAreaView } from 'react-native';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/Entypo';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import LinearGradient from 'react-native-linear-gradient';


class SignUpView extends React.Component {
  static navigationOptions = {
    title: 'Sign up',
  };

  constructor(props) {
    super(props);
  
    this.state = {
      email: "",
      password: "",
      name: "",
    };
  }

  checkUser = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        Alert.alert("Sign up", "You are already logged in!", [
          {text: 'OK', onPress: () => this.props.navigation.navigate('Home')},
        ]);
      }
    });
  }

  async register(email, password, name) {

    if (name == "") {
      Alert.alert("Sign up", "Name field is empty.");
      return;
    } else if (email == "") {
      Alert.alert("Sign up", "Email field is empty.");
      return;
    } else if (password == "") {
      Alert.alert("Sign up", "Password field is empty.");
      return;
    }

    try {
      await firebase.app().auth().createUserWithEmailAndPassword(email, password).then((res) => {
        firebase.auth().currentUser.updateProfile({displayName: name}).then(() => firebase.auth().signOut())
      }).then(() => {
        this.setState({email: "", password: "", name: ""});
        Alert.alert("Sign up", "You have been successfully registered!", [
          {text: 'OK', onPress: () => this.props.navigation.navigate('SignIn')},
        ]);
      });

    } catch (e) {
      console.log(e)
      Alert.alert("Sign up", e.message.split("] ")[1]);
    }
  }

  render() {
    return (
      <LinearGradient colors= {["rgba(95,189,189,0.45)", "rgba(255,255,255,1)"]} style= {{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
          <Image
            style={{width: "100%", height: 40, marginBottom: 50, marginTop: 30}}
            source={require('../assets/logo.png')}
            resizeMode= 'contain'
          />
          <TextInput
            style={{ height: 40, width: "90%", padding: 5, marginTop: 20, marginBottom: 30, paddingHorizontal: 20, borderBottomColor: 'gray', borderBottomWidth: 0.5}}
            onChangeText={name => this.setState({name})}
            placeholder={"First name"}
            value={this.state.name}
          />
          <TextInput
            style={{ height: 40, width: "90%", padding: 5, marginBottom: 30, paddingHorizontal: 20, borderBottomColor: 'gray', borderBottomWidth: 0.5}}
            onChangeText={email => this.setState({email})}
            placeholder={"Email"}
            value={this.state.email}
          />
          <TextInput
            style={{ height: 40, width: "90%", padding: 5, marginBottom: 30, paddingHorizontal: 20, borderBottomColor: 'gray', borderBottomWidth: 0.5}}
            onChangeText={password => this.setState({password})}
            placeholder={"Password"}
            value={this.state.password}
            secureTextEntry={true}
          />

          <TouchableOpacity style={[styles.button, {width: "60%", marginBottom: 30}]} onPress={() => this.register(this.state.email, this.state.password, this.state.name)}>
              <Icon name={"add-user"} size={20} color='white' />
              <Text style={{fontSize: 16, fontWeight: "600", color: "white", marginLeft: 5}}>Sign up</Text>
          </TouchableOpacity>
           <TouchableOpacity>
              <Text>Already have an account? <Text onPress={() => this.props.navigation.navigate('SignIn')} style={{color: "#00a1f1"}}>Sign in</Text></Text>
          </TouchableOpacity>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}


export default SignUpView;
import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  navbar: {
    position: "absolute",
    bottom: 20,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 64,
  },
  btnBack: {
    flex: 1,
  },
  navbarIconButton: {
    flex: 1,
    opacity: 0.66,
  },
  navbarIconButtonSelected: {
    flex: 1,
    opacity: 1.0,
  },
  button: {
    margin: 5,
    borderRadius: 7,
    padding: 10,
    paddingHorizontal: 20,
    minWidth: 150,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:'#00a1f1',
    color: "white",

  },
  buttonOutline: {
    margin: 5,
    borderRadius: 7,
    padding: 10,
    paddingHorizontal: 20,
    minWidth: 150,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0.5,
    borderColor: 'black'
  }
})
import React from 'react';
import { View, SafeAreaView, Text, Button, Image, TouchableOpacity, Alert, ImageBackground, Linking } from 'react-native';
import {NavbarButton} from '../components/others';
import Icon from 'react-native-vector-icons/Entypo';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/dynamic-links';
import '@react-native-firebase/auth';
import { styles } from './styles';


class AutoView extends React.Component {

  constructor(props) {
    super(props);

  }

  openLink = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {

        console.log("Don't know how to open URI: " + url);
      }
    });
  };
 
  render() {
    return (
      <ImageBackground style={{width: '100%', height: '100%'}} source={require('../assets/landingpage.png')}>
        <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{paddingHorizontal: 15, paddingVertical: 10, textAlign: 'center', fontSize: 14}}>Interested in an <Text style={{fontWeight: "700"}}>Auto and Home</Text> policy? Please visit our website to learn more: <Text style={{color: "#00a1f1"}} onPress={() => this.openLink("selectquote.com")}>selectquote.com</Text></Text>
        </SafeAreaView>
      </ImageBackground>
    );
  }
} 

export default AutoView;

import React from 'react';
import { View, SafeAreaView, Text, Button, Image, TouchableOpacity, Alert, ImageBackground, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import AutoView from './AutoView';
import LifeView from './LifeView';
import MedicareView from './MedicareView';

import HomeView from './HomeView';
import SignInView from './SignInView';
import SignUpView from './SignUpView';

class NavigationDrawerStructure extends React.Component {
  toggleDrawer = () => {
    this.props.navigationProps.toggleDrawer();
  };
  render() {
    return (
      <View style={{ flexDirection: 'row', marginLeft: 15 }}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          <Icon name={"menu"} size={20} color='white' />
        </TouchableOpacity>
      </View>
    );
  }
}

var SignInNav = createStackNavigator({
  SignInView: {
    screen: SignInView,
    navigationOptions: ({ navigation }) => ({
      title: 'Sign in',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#00a1f1',
      },
      headerTintColor: '#fff',
    }),
  },
}, {
  headerLayoutPreset: 'center'
})

var SignUpNav = createStackNavigator({
  SignUpView: {
    screen: SignUpView,
    navigationOptions: ({ navigation }) => ({
      title: 'Sign up',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
 
      headerStyle: {
        backgroundColor: '#00a1f1',
      },
      headerTintColor: '#fff',
    }),
  },
}, {
  headerLayoutPreset: 'center'
})

var HomeNav = createStackNavigator({
  HomeView: {
    screen: HomeView,
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
 
      headerStyle: {
        backgroundColor: '#00a1f1',
      },
      headerTintColor: '#fff',
    }),
  },
}, {
  headerLayoutPreset: 'center'
})

var AutoNav = createStackNavigator({
  AutoView: {
    screen: AutoView,
    navigationOptions: ({ navigation }) => ({
      title: 'Auto and Home Insurance',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
 
      headerStyle: {
        backgroundColor: '#00a1f1',
      },
      headerTintColor: '#fff',
    }),
  },
}, {
  headerLayoutPreset: 'center'
})

var LifeNav = createStackNavigator({
  LifeView: {
    screen: LifeView,
    navigationOptions: ({ navigation }) => ({
      title: 'Life Insurance',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
 
      headerStyle: {
        backgroundColor: '#00a1f1',
      },
      headerTintColor: '#fff',
    }),
  },
}, {
  headerLayoutPreset: 'center'
})

var MedicareNav = createStackNavigator({
  MedicareView: {
    screen: MedicareView,
    navigationOptions: ({ navigation }) => ({
      title: 'Supplemental Medicare Insurance',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
 
      headerStyle: {
        backgroundColor: '#00a1f1',
      },
      headerTintColor: '#fff',
    }),
  },
}, {
  headerLayoutPreset: 'center'
})



var DrawerStack = createDrawerNavigator({
  Home: {
    screen: HomeNav,
    navigationOptions: {
      drawerLabel: 'Home',
    },
  },
  SignIn: {
    screen: SignInNav,
    navigationOptions: {
      drawerLabel: 'Sign in'
    },
  },
  SignUp: {
    screen: SignUpNav,
    navigationOptions: {
      drawerLabel: 'Sign up',
    },
  },
  Auto: {
    screen: AutoNav,
    navigationOptions: {
      drawerLabel: 'Auto and Home Insurance',
    },
  },
  Life: {
    screen: LifeNav,
    navigationOptions: {
      drawerLabel: 'Life Insurance'
    },
  },
  Medicare: {
    screen: MedicareNav,
    navigationOptions: {
      drawerLabel: 'Supplemental Medicare Insurance',
    },
  },
}, {
  initialRouteName: 'Home',
  contentOptions: {
    activeTintColor: '#00a1f1',
  },
})


var DrawerNavigator = createStackNavigator({
  Drawer: { screen: DrawerStack, navigationOptions: { header: null } },
});

export default createAppContainer(DrawerNavigator);
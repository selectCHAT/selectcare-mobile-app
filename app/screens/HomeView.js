import React from 'react';
import { View, SafeAreaView, Text, Button, Image, TouchableOpacity, Alert, ImageBackground, Linking } from 'react-native';
import {NavbarButton} from '../components/others';
import Icon from 'react-native-vector-icons/Entypo';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/dynamic-links';
import '@react-native-firebase/auth';
import { styles } from './styles';


class HomeView extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      user: null
    };

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user})
      } else {
        this.setState({user: null})
      }
    });

  }

  authAction = () => {
    if (this.state.user != null) {
        Alert.alert("Logout", "Are you sure?", [
          {text: 'Yes', onPress: () => firebase.auth().signOut()},
          {text: 'No'},
        ]);
    } else {
      this.props.navigation.navigate('SignIn')
    }
  }

  openLink = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {

        console.log("Don't know how to open URI: " + url);
      }
    });
  };
 
  render() {
    return (
      <ImageBackground style={{width: '100%', height: '100%'}} source={require('../assets/landingpage.png')}>
        <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
          <Image
            style={{width: "100%", height: 40, marginBottom: 30, marginTop: 30}}
            source={require('../assets/logo.png')}
            resizeMode= 'contain'
          />
          <Text style={{padding: 15, textAlign: 'center', fontSize: 27, fontWeight: "700", marginBottom: 5, color: "#00a1f1"}}>Welcome to Selectquote Insurance Services{this.state.user != null && ", " + this.state.user.displayName}</Text>
          <Text style={{textAlign: 'center', fontSize: 24, fontWeight: "700", marginBottom: 15, color: "#00a1f1"}}>We shop, you save!</Text>
          <Text style={{paddingHorizontal: 15, paddingVertical: 10, textAlign: 'center', fontSize: 14}}>To shop for <Text style={{fontWeight: "700"}}>Life Insurance</Text>, please visit <Text style={{color: "#00a1f1"}} onPress={() => this.openLink("selectquote.com")}>selectquote.com</Text>,</Text>
          <Text style={{paddingHorizontal: 15, paddingVertical: 10, textAlign: 'center', fontSize: 14}}>To shop for <Text style={{fontWeight: "700"}}>Auto and Home insurance</Text> please visit <Text style={{color: "#00a1f1"}} onPress={() => this.openLink("selectquoteautoandhome.com")}>selectquoteautoandhome.com</Text>,</Text>
          <Text style={{paddingHorizontal: 15, paddingVertical: 10, textAlign: 'center', fontSize: 14}}>To shop for <Text style={{fontWeight: "700"}}>Supplemental Medicare coverage</Text> , please visit <Text style={{color: "#00a1f1"}} onPress={() => this.openLink("selectquotesenior.com")}>selectquotesenior.com</Text>.</Text>
          <View style={styles.navbar}>
            {this.state.user != null && <TouchableOpacity style={styles.button}>
              <Icon name={"phone"} size={20} color='white' />
              <Text style={{fontSize: 16, fontWeight: "600", color: "white", marginLeft: 5}}>Call me now</Text>
            </TouchableOpacity>}
            {this.state.user != null && <TouchableOpacity style={styles.button} onPress={()=> this.authAction()}>
              <Icon name={"log-out"} size={20} color='white' />
              <Text style={{fontSize: 16, fontWeight: "600", color: "white", marginLeft: 5}}>{this.state.user != null ? "Logout" : "Login"}</Text>
            </TouchableOpacity>}
          </View> 
        </SafeAreaView>
      </ImageBackground>
    );
  }
} 

export default HomeView;

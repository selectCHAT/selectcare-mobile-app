import React from 'react';
import { View, Text, Button, Image, TextInput, KeyboardAvoidingView, TouchableOpacity, Alert, Platform, SafeAreaView } from 'react-native';
import { styles } from './styles';
import Icon from 'react-native-vector-icons/Entypo';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import launchMailApp from "react-native-mail-launcher";
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';


class SignInView extends React.Component {
  static navigationOptions = {
    title: 'Sign in',
  };

  constructor(props) {
    super(props);
  
    this.state = {
      email: "",
    };
  }

  checkUser = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        Alert.alert("Sign in", "You are already logged in!", [
          {text: 'OK', onPress: () => this.props.navigation.navigate('Home')},
        ]);
      }
    });
  }

  async login(email) {

    if (email == "") {
      Alert.alert("Sign in", "Email field is empty.");
      return;
    }

    AsyncStorage.setItem('@email', email);

    try {
      await firebase.app().auth().sendSignInLinkToEmail(email,{
        url: "https://localhost",
        handleCodeInApp: true,
        dynamicLinkDomain: "selectquote.page.link",
        iOS: {
          bundleId: 'com.selectquote',
        },
        android: {
          installApp: true,
          packageName: 'com.policy',
        },
      }).then((res) => {
        this.setState({email: ""});
        Alert.alert("Sign in", "Login link has been successfully sent! Please, open the link to login into your account.", [
          {text: 'Open mail app', onPress: () => {
            this.props.navigation.navigate('Home');
            launchMailApp();
          }},
          {text: 'OK', onPress: () => this.props.navigation.navigate('Home')},
        ]);
      });

    } catch (e) {
      Alert.alert("Sign in", e.message.split("] ")[1]);
    }
  }

  render() {
    return (
      <LinearGradient colors= {["rgba(95,189,189,0.45)", "rgba(255,255,255,1)"]} style= {{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>

          <Image
            style={{width: "100%", height: 40, marginBottom: 50, marginTop: 30}}
            source={require('../assets/logo.png')}
            resizeMode= 'contain'
          />
          <TextInput
            style={{ height: 40, width: "90%", padding: 5, marginTop: 60, marginBottom: 30, paddingHorizontal: 20, borderBottomColor: 'gray', borderBottomWidth: 0.5 }}
            onChangeText={email => this.setState({email})}
            placeholder={"Email"}
            value={this.state.email}
          />
          <TouchableOpacity style={[styles.button, {width: "60%", marginBottom: 30}]} onPress={() => this.login(this.state.email)}>
              <Icon name={"login"} size={20} color='white' />
              <Text style={{fontSize: 16, fontWeight: "600", color: "white", marginLeft: 5}}>Sign in</Text>
          </TouchableOpacity>
          
          <TouchableOpacity>
              <Text>Don't you have an account? <Text onPress={() => this.props.navigation.navigate('SignUp')} style={{color: "#00a1f1"}}>Sign up</Text></Text>
          </TouchableOpacity>
        </SafeAreaView>
      </LinearGradient>

    );
  }
}


export default SignInView;
import { StyleSheet,Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#343a40',
        zIndex: 100,
        width, height,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        flex: 0.15,
        fontSize: 20,
        width: 200,
        textAlign: 'center',
        color: '#fff'
    },
    activityIndicator: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    internettext: {
        fontSize: 20,
        width: width*0.8,
        textAlign: 'center',
        color: '#fff'
    }  
})
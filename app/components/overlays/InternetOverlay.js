import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { styles } from './styles';

export default class InternetOverlay extends Component {

    constructor(props) {
        super(props);
      
      this.noInternettext = 'No Internet available. Please, check your connection!';
    }

    render() {
        return (
            <View style={styles.overlay}>
                <Image
                  style={{width: 100, height: 100, marginBottom: 20}}
                  resizeMode="contain"
                  source={require('../../assets/logo.png')}
                />
                <Text style={styles.internettext}>{this.noInternettext}</Text>
            </View>
        );
    }
}
import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  navbarIcons: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  navbarIcon: {
    alignItems: 'center'
  },
  navbarIconText: {
    marginTop: 2,
    fontSize: 12,
    color: '#ffffff',
  },
});

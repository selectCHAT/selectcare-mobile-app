import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { styles } from './styles';

export default class NavbarButton extends React.Component {
  render() {
    const {icon, label} = this.props;
    return(
      <View style={styles.navbarIcons}>
        <Icon name={`${icon}`} size={24} color='#ffffff' />
        <Text style={styles.navbarIconText}>{`${label}`}</Text>
      </View>
    );
  }
}